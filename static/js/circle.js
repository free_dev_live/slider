function initSlider(sliderID, arrayOfValues, valueBetween, boolMemory) {
    var arr;
    var step = 1;
    var defaultF = '';
    var maxValue = 0;
    if (valueBetween){
        step ++;
        var lengthTempArray = arrayOfValues.length-1;
        var tmpArray = [];
        var resultArray = [];
        for(var i = 0; i < lengthTempArray; i++){
            var tempLess = parseInt(arrayOfValues[i].match(/\d+/g).join(''));
            var tempMore = parseInt(arrayOfValues[i+1].match(/\d+/g).join(''));
            var tempLessFormat = arrayOfValues[i+1].match(/[A-Z]/g);
            var tempMoreFormat = arrayOfValues[i+1].match(/[A-Z]/g);
            if((tempLess > tempMore) && (tempLessFormat != null) && (tempMoreFormat != null)){
                var tmpFL = tempLessFormat.join('');
                var tmpFM = tempMoreFormat.join('');
                if (tmpFL != tmpFM){
                    defaultF = "GB";
                }
                else{
                    defaultF = tmpFL
                }
                tmpArray.push((tempLess+125) + "GB");
            }
            else{
                if((tempLessFormat != null) && (tempMoreFormat != null)){
                    var tmpFL = tempLessFormat.join('');
                    var tmpFM = tempMoreFormat.join('');
                    if(tmpFM == tmpFL){
                        defaultF = tmpFM
                    }
                }
                var pushed = (tempLess+tempMore)/2;
                if (pushed != 1.5){
                    tmpArray.push(pushed + defaultF);
                }
                else{
                    tmpArray.push('')
                }

            }
        }
        for(var i = 0; i < lengthTempArray; i++){
            resultArray.push(arrayOfValues[i]);
            resultArray.push(tmpArray[i])
        }
        resultArray.push(arrayOfValues[lengthTempArray]);
        arr = resultArray.slice();
        maxValue = arr.length;
    }
    else{
        arr = arrayOfValues.slice();
        maxValue = arr.length;
    }
    var slider = $(`#${sliderID}`);
    slider.roundSlider({
        sliderType: "min-range",
        handleShape: "square",
        startAngle: -30,
        endAngle: "+240",
        radius: 100,
        mouseScrollAction: true,
        min: 1,
        max: maxValue,
        width: 2,
        value: 1,
        step: step,
        arr: arr,
        change: "tooltipVal",
        between: valueBetween,
        memory: boolMemory
    });
    slider.roundSlider("option", {"step": step});
    slider.css({'top': 50, 'left': 50});
}


 function tooltipVal(args) {
    var temp = args.options.arr;
    onDrag(args);
    return temp[args.value - 1];
}


function replaceBorders(sliderID, radius){
  var slider = $(`#${sliderID}`);
  var width = slider.width();
  var height = slider.height();
  var innerContainer = slider.find('.rs-inner-container');
  innerContainer.append('<div class="new threeFull"><div class="inner"></div></div>')
  var outer = innerContainer.find('.new');
  var inner = innerContainer.find('.inner');
  outer.css({'width': width, 'height': height});
  inner.css({'width': width-radius, 'height': height-		radius, 'top': radius/2, 'left': radius/2});
  slider.find('.rs-handle').css("border-width", `2px ${width/2}px 2px 4px`)
}


function circularText(id,txt,classIndex) {
	$(`#${id}`).append(`<div class="txtcontainer"><div class="circTxt"></div></div>`);

  var radius = $(`#${id}`).width()/2;
  $(`#${id} .txtcontainer`).css({'width': radius, 'height' : radius, 'top': -radius*2.4, 'left': radius/2})
  classIndex = document.getElementsByClassName("circTxt")[classIndex];

  var deg = 260/ txt.length,
    origin = -127;

  txt.forEach((ea) => {
    ea = `<div style='height:${radius*1.4}px;position:absolute;transform:rotate(${origin}deg);transform-origin:0% 100%'><p style='width: 40px; transform: rotate(${-origin}deg)'>${ea}</p></div>`;
    classIndex.innerHTML += ea;
    origin += deg;
  });
}


function createInput(id, typeString, memory){
    var init_value_list = $(`#${id}`).roundSlider("option", "arr");
    var initiated_value = init_value_list[0].match(/\d+/g).join('');
    var select = '';
    if (memory){
        select = `<select class="selectControll" name="${id}_select" id="${id}_select" onchange="inputControl({'inputObj':this,'idInput': id,'Value': value})">
        <option value="GB" selected="selected">GB</option>
        <option value="TB">TB</option>
</select>`
    }
    $(`#${id} .rs-inner-container`)
        .append(`<div class="inputContainer"><input id="${id}_val" name="${id}_val" type="text" value="${initiated_value}"
 class="inputField" onchange="inputControl({'inputObj': this, 'idInput': id, 'Value': value})"><div class="inputTextContainer">${typeString}</div>${select}</div>`);
}


function inputControl(args){
        var sliderID =  args['idInput'].split('_')[0];
        var input = $(`#${sliderID}_val`);
        var inputVal = input.val();
        var selector = $(`#${sliderID}_select`);
        var objectSlider = $(`#${sliderID}`);
        var between = objectSlider.roundSlider("option", "between");
        if (args.hasOwnProperty('idInput') && (between == true)){
            objectSlider.roundSlider("option", {"step": 1})

        }
        var percentOfLimit = 0.1;
        if (objectSlider.roundSlider("option", "memory") == true){
            percentOfLimit = 0.2;
        }
        var list = objectSlider.roundSlider("option", "arr");
        console.log(list)
        var listLength = list.length;
        var selected_val = selector.val();
        var gbLimit = findMaxGB(list);
        var inputValFormat = inputVal.match(/^\D+/g);
        if (inputValFormat != null){
            inputValFormat = inputValFormat.join('');
        }
        if (inputVal == null || inputVal == undefined || inputVal == ''){
            objectSlider.roundSlider("option", { "value": 0 });
            selector.val('GB')
        }
        else{
            inputVal = parseInt(inputVal.match(/\d+/g).join(''));

            for(var i = 0; i < listLength; i++){
                var less = inputVal - inputVal*percentOfLimit;
                var more = inputVal + inputVal*percentOfLimit;
                if ((less+selected_val < list[i] && list[i] < more+selected_val)||(less < list[i] && list[i] < more)){
                    console.log('hi');
                objectSlider.roundSlider("option", { "value": i+1 });
                return null;
                }
            }
            for (var i = 0; i < listLength; i++){
                if (list[i] == ""){continue}
                var format = list[i].match(/[A-Z]+/g);
                if (format != null){
                    format = format.join('');
                }
                var tempListValue = list[i].match(/\d+/g).join('');
                if(inputVal > gbLimit && selected_val == "GB"){
                    input.val("1");
                    selector.val("TB")
                    return inputControl(args)
                }

                    if((inputVal > tempListValue)&& (inputValFormat ==  format || selected_val == format)){
                        objectSlider.roundSlider("option", { "value": i + 2  });
                        input.val(inputVal);
                }
            }

        }
}


function onDrag(args){
    var Val = args.options.arr[args.options.value-1];
        var neededVal = Val.match(/\d+/g);
        console.log(args.options.arr);
        var selectorNeededVal = Val.match(/[A-Z]/g);
        if (selectorNeededVal != null || selectorNeededVal != undefined){
            $(`#${args.id}_select`).val(selectorNeededVal.join(''))
        }
        $(`#${args.id}_val`).val(neededVal.join(''));
        if (args.options.between == true){
            $(`#${args.id}`).roundSlider("option", {"step": 2});
        }

        console.log("prev")
        return null
}

function findMaxGB(array){
    var length = array.length;
    var max = 0;
    for (var i = 0 ; i < length; i++){
        var tempVal = array[i].match(/\d+/g);
        var temp = '';
        if (tempVal != null){
            temp = parseInt(tempVal.join(''));
        }
        var tempF = array[i].match(/[A-Z]/g);
        if (tempF != null){
            tempF = tempF.join('');
        }
        if (max < temp && tempF == "GB"){
            max = temp
        }
    }
    return max;
}


function createRoundSlider(id, values_array, diameter, numberOfSlider, typeString, boolMemory, betweenValue){
    initSlider(id, values_array, betweenValue, boolMemory);
    replaceBorders(id, diameter);
    circularText(id, values_array, numberOfSlider);
    createInput(id, typeString, boolMemory);
}

$(document).ready(function () {
    createRoundSlider('handle1',['1', '2', '6', '8', '12', '16', '20', '24', '32', '40', '48'],
        5, 0, 'CORES', false, true);
createRoundSlider('handle2',['2', '4', '8', '16', '32', '64', '128', '192', '256', '324', '512'],
    5, 1, 'RAM', false, true);
createRoundSlider('handle3',['50GB', '100GB', '250GB', '500GB', '750GB', '1TB', '2TB', '4TB', '8TB', '16TB', '32TB'],
    5, 2, 'FLASH', true, true);
createRoundSlider('handle4',['50GB','500GB', '1TB', '2TB', '4TB', '5TB', '8TB', '10TB', '16TB', '32TB', '64TB'],
    5, 3, "HDD", true, true);
});
