##Instruction

* Include to your HTML File the following links

```
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/roundSlider/1.3.2/roundslider.min.css" rel="stylesheet"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/roundSlider/1.3.2/roundslider.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
```

* Include **circle.js** && **slide.css** at the end of the file

* Initiate slider 
    
    
```
$(document).ready(function () {
createRoundSlider(id,listOfStringValues,
        diameter, numberOfSlider, Name, Memory, BeetweenFlag);})
```
###Example

```
$(document).ready(function () {
    createRoundSlider('handle1',['1', '2', '6', '8', '12', '16', '20', '24', '32', '40', '48'],
        5, 0, 'CORES', false, false);
createRoundSlider('handle2',['2', '4', '8', '16', '32', '64', '128', '192', '256', '324', '512'],
    5, 1, 'RAM', false, false);
createRoundSlider('handle3',['50GB', '100GB', '250GB', '500GB', '750GB', '1TB', '2TB', '4TB', '8TB', '16TB', '32TB'],
    5, 2, 'FLASH', true, false);
createRoundSlider('handle4',['50GB','500GB', '1TB', '2TB', '4TB', '5TB', '8TB', '10TB', '16TB', '32TB', '64TB'],
    5, 3, "HDD", true, false);
});
```

* ``id`` - is and id of slider's div
* ``listOfStringValues`` - list of values which will be printed around and set as range values
* ``diameter`` - is an coloured arc diameter
* ``numberOfSlider`` - what number in html it is
* ``Name`` - word which will be printed near input field
* ``Memory`` - set if it memory slider or nor if true will create dropdown list of ``GB`` and ``TB`` values
* ``BeetweenFlag`` - is boolean value as ``Memory`` indicate should slider stop in between

####Beetween flags availability

* Disabled beetween values

```
createRoundSlider('handle1',['1', '2', '6', '8', '12', '16', '20', '24', '32', '40', '48'],
        5, 0, 'CORES', false, false);
```

* Enabled between values

```
createRoundSlider('handle1',['1', '2', '6', '8', '12', '16', '20', '24', '32', '40', '48'],
        5, 0, 'CORES', false, true);
```